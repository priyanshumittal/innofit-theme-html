	// OWL SLIDER CUSTOM JS

	$(document).ready(function() {
		
		// Tooltip Js
		$(function () {
			$('[data-toggle="tooltip"]').tooltip()
		});
	
		// Accodian Js
		function toggleIcon(e) {
			$(e.target)
			.prev('.panel-heading')
			.find(".more-less")
			.toggleClass('fa-plus-square-o fa-minus-square-o');
		}
		$('.panel-group').on('hidden.bs.collapse', toggleIcon);
		$('.panel-group').on('shown.bs.collapse', toggleIcon);
		
	
		$("#slider-carousel").owlCarousel({
			navigation : true, // Show next and prev buttons	
			autoplay: true,
			autoplayTimeout: 3000,
			autoplayHoverPause: true,
			smartSpeed: 800,			
			singleItem:true,
			loop:true, // loop is true up to 1199px screen.
			nav:true, // is true across all sizes
			margin:0, // margin 10px till 960 breakpoint
			responsiveClass:true, // Optional helper class. Add 'owl-reponsive-' + 'breakpoint' class to main element.
			items: 1,
			dots: false,
			navText: ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"]
		});
				
				
		$("#shop-carousel").owlCarousel({
			navigation : true, // Show next and prev buttons		
			autoplay: true,
			autoplayTimeout: 3000,
			autoplayHoverPause: true,
			smartSpeed: 700,
		
			loop:true, // loop is true up to 1199px screen.
			nav:false, // is true across all sizes
			margin:30, // margin 10px till 960 breakpoint
			autoHeight: true,
			responsiveClass:true, // Optional helper class. Add 'owl-reponsive-' + 'breakpoint' class to main element.
			//items: 3,
			dots: true,
			navText: ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
			responsive:{ 	
				480:{ items:1 },
				768:{ items:2 },
				1000:{ items:3 }			
			}
		});	
				
				
		$("#testimonial-carousel").owlCarousel({
			//navigation : true, // Show next and prev buttons		
			autoplay: true,
			autoplayTimeout: 3000,
			autoplayHoverPause: true,
			smartSpeed: 1000,

			loop:true, // loop is true up to 1199px screen.
			nav:true, // is true across all sizes
			margin:0, // margin 10px till 960 breakpoint
			autoHeight: true,
			responsiveClass:true, // Optional helper class. Add 'owl-reponsive-' + 'breakpoint' class to main element.
			//items: 1,
			dots: true,
			navText: ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
			
			responsive:{ 
				100:{ items:1 },	
				480:{ items:1 },
				768:{ items:2 },
				1000:{ items:3 }				
				
			}
		});
			
		$("#team-carousel").owlCarousel({
			navigation : true, // Show next and prev buttons		
			autoplay: true,
			autoplayTimeout: 2000,
			autoplayHoverPause: true,
			smartSpeed: 700,		
		
			loop:true, // loop is true up to 1199px screen.
			nav:true, // is true across all sizes
			margin:0, // margin 10px till 960 breakpoint
			
			responsiveClass:true, // Optional helper class. Add 'owl-reponsive-' + 'breakpoint' class to main element.
			//items: 5,
			dots: false,
			navText: ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
			responsive:{
				100:{ items:1 },
				480:{ items:1 },
				768:{ items:2 },
				1000:{ items:4 }
			}
		});	

		$("#blog-carousel").owlCarousel({
			navigation : true, // Show next and prev buttons		
			autoplay: true,
			autoplayTimeout: 2000,
			autoplayHoverPause: true,
			smartSpeed: 700,		
		
			loop:true, // loop is true up to 1199px screen.
			nav:false, // is true across all sizes
			margin:30, // margin 10px till 960 breakpoint
			
			responsiveClass:true, // Optional helper class. Add 'owl-reponsive-' + 'breakpoint' class to main element.
			//items: 5,
			dots: true,
			navText: ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
			responsive:{
				100:{ items:1 },
				480:{ items:1 },
				768:{ items:2 },
				1000:{ items:3 }
			}
		});		
		
		$("#clients-carousel").owlCarousel({
			navigation : true, // Show next and prev buttons		
			autoplay: true,
			autoplayTimeout: 2000,
			autoplayHoverPause: true,
			smartSpeed: 700,
		
			loop:true, // loop is true up to 1199px screen.
			nav:false, // is true across all sizes
			margin:30, // margin 10px till 960 breakpoint
			
			responsiveClass:true, // Optional helper class. Add 'owl-reponsive-' + 'breakpoint' class to main element.
			//items: 5,
			dots: false,
			navText: ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],		
			responsive:{ 
				100:{ items:1 },	
				500:{ items:2 },
				768:{ items:3 },
				1000:{ items:5 }	
			}
		});
		
		
		$("#instagram-carousel").owlCarousel({
			navigation : true, // Show next and prev buttons		
			autoplay: true,
			autoplayTimeout: 2000,
			autoplayHoverPause: true,
			smartSpeed: 1200,
		
			loop:true, // loop is true up to 1199px screen.
			nav:false, // is true across all sizes
			margin:0, // margin 10px till 960 breakpoint
			
			responsiveClass:true, // Optional helper class. Add 'owl-reponsive-' + 'breakpoint' class to main element.
			//items: 5,
			dots: false,
			navText: ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
			responsive:{ 
				100:{ items:1 },	
				480:{ items:1 },
				600:{ items:2 },
				768:{ items:4 },
				1000:{ items:6 }
			}
		
		});
		
		
		 
	});
	